#include <iostream>
#include <boost/test/minimal.hpp>
#include <libmathduel/player/PlayerList.h>
#include <libmathduel/player/Player.h>

int test_main(int, char* [])
{
    PlayerList& pl = PlayerList::getInstance();
    std::string player1_name("Player 1");

    pl.put(PPlayer(new Player(player1_name)));
    BOOST_REQUIRE(pl.isPresent(player1_name));

    PPlayer player = pl.get(player1_name);
    BOOST_CHECK(player->getNickName() == player1_name);

    pl.pop(player1_name);
    BOOST_CHECK(!pl.isPresent(player1_name));
    return 0;
}

