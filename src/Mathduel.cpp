#include <Wt/WApplication>
#include <Wt/WContainerWidget>
#include <Wt/WEnvironment>
#include <Wt/WServer>
#include <Wt/WTimer>
#include <Wt/WPushButton>
#include <Wt/WText>
#include "libmathduel/server/MathduelServer.h"
#include "libmathduel/view/MathduelWidget.h"

using namespace Wt;

/*! \brief Mathduel  application.
 */
class MathduelApplication : public WApplication
{
public:
  /*! \brief Create a new instance.
   */
  MathduelApplication(const WEnvironment& env, MathduelServer& server);

private:
   MathduelServer& server_;
   Wt::WText *javaScriptError_;
   const WEnvironment& env_;
   Wt::WTimer *timer_;

  void javaScriptTest();
  void emptyFunc();
};

MathduelApplication::MathduelApplication(const WEnvironment& env,
				 MathduelServer& server)
  : WApplication(env),
    server_(server),
    env_(env)
{
  setTitle("Mathduel");
  useStyleSheet("mathduelapp.css");

  messageResourceBundle().use(appRoot() + "mathduel");

  javaScriptTest();

  root()->addWidget(new WText(WString::tr("introduction")));

  MathduelWidget *mathduelWidget =
      new MathduelWidget(server_, root());
  mathduelWidget->setStyleClass("chat");
  mathduelWidget->resize(WLength::Auto, 300);
}

void MathduelApplication::javaScriptTest()
{
  if(!env_.javaScript()){
    javaScriptError_ = new WText(WString::tr("serverpushwarning"), root());

    // The 5 second timer is a fallback for real server push. The updated
    // server state will piggy back on the response to this timeout.
    timer_ = new Wt::WTimer(root());
    timer_->setInterval(5000);
    timer_->timeout().connect(this, &MathduelApplication::emptyFunc);
    timer_->start();
  }
}


void MathduelApplication::emptyFunc()
{}

WApplication *createApplication(const WEnvironment& env,
				MathduelServer& server)
{
  return new MathduelApplication(env, server);
}

int main(int argc, char **argv)
{
  Wt::WServer server(argv[0]);
  MathduelServer mathduelServer(server);

  server.setServerConfiguration(argc, argv, WTHTTP_CONFIGURATION);

  /*
   * We add two entry points: one for the full-window application,
   * and one for a widget that can be integrated in another page.
   */
  server.addEntryPoint(Wt::Application,
		       boost::bind(createApplication, _1,
				   boost::ref(mathduelServer)));
   if (server.start()) {
    int sig = Wt::WServer::waitForShutdown();
    std::cerr << "Shutting down: (signal = " << sig << ")" << std::endl;
    server.stop();
  }
}
