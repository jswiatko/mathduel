#include <random>
#include <chrono>
#include <libmathduel/common/Random.h>

#include <iostream>

std::default_random_engine Random::_generator(std::chrono::system_clock::now().time_since_epoch().count());

int Random::roll(int min, int max)
{
    std::uniform_int_distribution<int> distribution(min,max);
    return distribution(_generator);
}
