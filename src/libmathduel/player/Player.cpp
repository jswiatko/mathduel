#include <libmathduel/player/Player.h>

Player::Player(const std::string nickName) :
    state(OFFLINE), _nickName(nickName), _score(0) {}

std::string Player::getNickName()
{
    return _nickName;
}

int Player::getScore()
{
    return _score;
}

void Player::awardPoints(int points)
{
    _score += points;
}
