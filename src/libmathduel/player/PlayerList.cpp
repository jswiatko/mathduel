#include <utility>
#include <iostream>
#include <iomanip>
#include <boost/thread.hpp>
#include <libmathduel/player/PlayerList.h>

PlayerList& PlayerList::getInstance()
{
    static PlayerList _instance;
    return _instance;
}

PlayerList::PlayerList() {}

void PlayerList::put(PPlayer player)
{
    boost::lock_guard<boost::mutex> lock(_mutex);
    _players.insert( std::pair<std::string, PPlayer>(player->getNickName(), player) );
}

PPlayer PlayerList::get(std::string nickname)
{
    boost::lock_guard<boost::mutex> lock(_mutex);
    return _players[nickname];
}

PPlayer PlayerList::pop(std::string nickname)
{
    boost::lock_guard<boost::mutex> lock(_mutex);
    PPlayer player_ptr = _players[nickname];
    _players.erase(nickname);
    return player_ptr;
}

bool PlayerList::isPresent(std::string nickname)
{
    boost::lock_guard<boost::mutex> lock(_mutex);
    return (_players.find(nickname) != _players.end());
}

void PlayerList::printAll()
{
    boost::lock_guard<boost::mutex> lock(_mutex);
    std::cout << std::setfill('_') << std::setw(16) << "Nickname" << "_|_" <<
                 std::setw(5) << "Pts" << "_|_State____" << std::setfill(' ') << std::endl;
    for(std::pair<std::string, PPlayer> entry : _players)
    {
        PPlayer p = entry.second;
        std::cout << std::setw(16) << p->getNickName() << " | " <<
                     std::setw(5) << p->getScore() << " | " <<
                     (std::string[]){ "OFFLINE", "IDLE", "WAITING", "PLAYING" }[p->state] << std::endl;
    }
}
