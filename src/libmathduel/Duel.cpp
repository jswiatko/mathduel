#include <boost/thread.hpp>
#include <libmathduel/Duel.h>
#include <libmathduel/expression/ExpressionBuilder.h>
#include <libmathduel/player/PlayerList.h>

Duel::Duel(const std::string &duelName, const std::string &hostUserName, long maxRound) :
     _duelName(duelName), _hostReady(false), _joinedReady(false), _resultSubmitted(false),
     _currentRound(0), _maxRounds(maxRound), _hostPlayerPoints(0), _joinedPlayerPoints(0)
{
    if(!PlayerList::getInstance().isPresent(hostUserName))
        PlayerList::getInstance().put(PPlayer(new Player(hostUserName)));

    _hostPlayer = PlayerList::getInstance().get(hostUserName);
}

std::string Duel::getDuelName()
{
    boost::lock_guard<boost::mutex> lock(_mutex);
    return _duelName;
}

bool Duel::isDuelAvailable()
{
    boost::lock_guard<boost::mutex> lock(_mutex);
    return _joinedPlayer == nullptr;
}

std::string Duel::getHostUserName()
{
    boost::lock_guard<boost::mutex> lock(_mutex);
    if(!_hostPlayer)
        return "";
    else
        return _hostPlayer->getNickName();
}

std::string Duel::getJoinedUserName()
{
    boost::lock_guard<boost::mutex> lock(_mutex);
    if(!_joinedPlayer)
        return "";
    else
    return _joinedPlayer->getNickName();
}

long Duel::getHostUserScore()
{
    boost::lock_guard<boost::mutex> lock(_mutex);
    return _hostPlayerPoints;
}

long Duel::getJoinedUserScore()
{
    boost::lock_guard<boost::mutex> lock(_mutex);
    return _joinedPlayerPoints;
}

long Duel::getCurrentRound()
{
    boost::lock_guard<boost::mutex> lock(_mutex);
    return _currentRound;
}

long Duel::getMaxRounds()
{
    boost::lock_guard<boost::mutex> lock(_mutex);
    return _maxRounds;
}

bool Duel::joinGame(const std::string& joinedUserName)
{
    boost::lock_guard<boost::mutex> lock(_mutex);
    if(_joinedPlayer)
        return false;

    if(!PlayerList::getInstance().isPresent(joinedUserName))
        PlayerList::getInstance().put(PPlayer(new Player(joinedUserName)));

    _joinedPlayer = PlayerList::getInstance().get(joinedUserName);
    return true;
}

/*! \brief Powierdzenie gotowości
* Ustawia pole isReady odpowiedniemu użytkonikowi identyfikowanegu loginem
* readyUserName. Gdy oboje gracze potwierzą swoją gotowość numer rundy ustawiany jest na 1.
*/
void Duel::setUserReadyForNextRound(const std::string& readyUserName)
{
    boost::lock_guard<boost::mutex> lock(_mutex);

    if(readyUserName == _hostPlayer->getNickName())
        _hostReady = true;
    else if(readyUserName == _joinedPlayer->getNickName())
        _joinedReady = true;

    if(_hostReady && _joinedReady)
        beginNextRound();
}

void Duel::beginNextRound()
{
    _resultSubmitted = false;
    if(++_currentRound <= _maxRounds)
        _currentExpression = ExpressionBuilder::makeExpression(3);
    else
    {
        _hostPlayer->awardPoints(_hostPlayerPoints);
        _joinedPlayer->awardPoints(_joinedPlayerPoints);
        ;//TODO daj znac ze koniec gry
    }
}

void Duel::setResultSubmitted(const bool value)
{
    boost::lock_guard<boost::mutex> lock(_mutex);
    _resultSubmitted = value;
}

bool Duel::isUserReady(const std::string& userName)
{
    boost::lock_guard<boost::mutex> lock(_mutex);

    if(userName == getHostUserName())
        return _hostReady;
    else if(userName == getJoinedUserName())
        return _joinedReady;
    else
        return false;
}

bool Duel::isResultSubmitted()
{
    boost::lock_guard<boost::mutex> lock(_mutex);
    if(_resultSubmitted)
        return true;
    else
        return false;
}

/*! \brief Zwraca string z poleceniem zadania, które znajduje się aktualnie w
* currentExpression. Gdy numer rundy znajduje sie w zakresie [1,maxRound]
* możliwe jest odpytywanie o Expression.
*/
std::string Duel::getExpression()
{
    boost::lock_guard<boost::mutex> lock(_mutex);
    if(!_currentExpression)
        return "";
    else
        return _currentExpression->string();
}

/*! \brief Zatwierdzenie odpowiedzi
*
* Po otrzymaniu odpowiedzi sprawdza jej poprawność i nadaje nadawcy
* (identyfikowanemu po loginie) odpowiednią liczbę punktów.
* Np. +1 za poprawną odpowiedź, -1 za błędną odpowiedź. Jeśli nie
* wygenerowano już maksymalnej liczby Expression dla pojedynczego Duel
*  (określone czez maxRound), generowane jest następne Expression
* i ustawiane jest jako currentExpression.
* Zwraca true, jeśli odpowiedź poprawna, false w przeciwnym wypadku.
*/
bool Duel::submitAnswer(const std::string& userName, double value)
{
    boost::lock_guard<boost::mutex> lock(_mutex);

    _hostReady = false;
    _joinedReady = false;
    if(value == _currentExpression->value()) {
        if(userName == _hostPlayer->getNickName())
            _hostPlayerPoints++;
        else if(userName == _joinedPlayer->getNickName())
            _joinedPlayerPoints++;
        return true;
    }
    else {
        if(userName == _hostPlayer->getNickName())
            _hostPlayerPoints--;
        else if(userName == _joinedPlayer->getNickName())
            _joinedPlayerPoints--;
        return false;
    }
}
