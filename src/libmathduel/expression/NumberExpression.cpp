#include <sstream>
#include <string>

#include <libmathduel/common/Random.h>
#include <libmathduel/expression/NumberExpression.h>

NumberExpression::NumberExpression()
{
    _value = (double)Random::roll(0, _max*_scale) / _scale;
}

NumberExpression::NumberExpression(double value) : _value(value) {}

double NumberExpression::value()
{
    return _value;
}

std::string NumberExpression::string()
{
    std::stringstream str;
    str << _value;
    return str.str();
}

int NumberExpression::level()
{
    return 10;
}
