#include <libmathduel/expression/Addition.h>

Addition::Addition() {}

Addition::Addition(PExpression left, PExpression right) :
    OperationExpression(left, right) {}

Addition::~Addition() {}

double Addition::value()
{
    return _left->value() + _right->value();
}

int Addition::level()
{
    return 2;
}

char Addition::operatorChar()
{
    return '+';
}
