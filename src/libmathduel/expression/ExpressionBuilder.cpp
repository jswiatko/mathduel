#include <libmathduel/common/Random.h>
#include <libmathduel/expression/Expression.h>
#include <libmathduel/expression/NumberExpression.h>
#include <libmathduel/expression/ExpressionBuilder.h>
#include <libmathduel/expression/Addition.h>
#include <libmathduel/expression/Multiplication.h>
#include <libmathduel/expression/Subtraction.h>

#include <iostream>


PExpression ExpressionBuilder::makeExpression(int numbers)
{
    if(numbers == 1)
        return PNumberExpression(new NumberExpression());

    numbers++;

    POperationExpression expression = randomOperationExpression();
    fillExpression(expression, numbers);

    return expression;
}

int ExpressionBuilder::fillExpression(POperationExpression expression, int numbers)
{
    numbers--;

    if(numbers <= 2)
    {
        expression->_left = PNumberExpression(new NumberExpression());
        expression->_right = PNumberExpression(new NumberExpression());
        return 0;
    }

    POperationExpression opEx = randomOperationExpression();
    numbers = fillExpression(opEx, numbers);
    expression->_left = opEx;

    if(numbers <= 1)
    {
        expression->_right = PNumberExpression(new NumberExpression());
        return 0;
    }

    opEx = randomOperationExpression();
    numbers = fillExpression(opEx, numbers);
    expression->_right = opEx;

    return numbers;
}

POperationExpression ExpressionBuilder::randomOperationExpression()
{
    switch(Random::roll(1, _operationsCount))
    {
        case 1:
            return PAddition(new Addition());
        break;

        case 2:
            return PMultiplication(new Multiplication());
        break;

        case 3:
            return PSubtraction(new Subtraction());
        break;
    }
    return nullptr;
}
