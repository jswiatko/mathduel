#include <libmathduel/expression/Subtraction.h>

Subtraction::Subtraction() {}

Subtraction::Subtraction(PExpression left, PExpression right) :
    OperationExpression(left, right) {}

Subtraction::~Subtraction() {}

double Subtraction::value()
{
    return _left->value() - _right->value();
}

int Subtraction::level()
{
    return 2;
}

char Subtraction::operatorChar()
{
    return '-';
}

std::string Subtraction::string()
{
    std::string str("");
    if(_left->level() < this->level())
    {
        str += "( ";
        str += _left->string() + " )";
    }
    else
        str += _left->string();

    str += " ";
    str += this->operatorChar();
    str += " ";

    if(_right->level() <= this->level())
    {
        str += "( ";
        str += _right->string() + " )";
    }
    else
        str += _right->string();

    return str;
}
