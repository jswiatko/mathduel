#include <libmathduel/expression/Multiplication.h>

Multiplication::Multiplication() {}

Multiplication::Multiplication(PExpression left, PExpression right) :
    OperationExpression(left, right) {}

Multiplication::~Multiplication() {}

double Multiplication::value()
{
    return _left->value() * _right->value();
}

int Multiplication::level()
{
    return 3;
}

char Multiplication::operatorChar()
{
    return '*';
}
