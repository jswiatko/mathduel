#include <libmathduel/expression/OperationExpression.h>

OperationExpression::OperationExpression() {}
OperationExpression::OperationExpression(PExpression left, PExpression right) : _left(left), _right(right) {}
OperationExpression::~OperationExpression() {}

std::string OperationExpression::string()
{
    std::string str("");
    if(_left->level() < this->level())
    {
        str += "( ";
        str += _left->string() + " )";
    }
    else
        str += _left->string();

    str += " ";
    str += this->operatorChar();
    str += " ";

    if(_right->level() < this->level())
    {
        str += "( ";
        str += _right->string() + " )";
    }
    else
        str += _right->string();

    return str;
}
