#include "libmathduel/server/MathduelServer.h"
#include <Wt/WServer>

#include <iostream>
#include <boost/lexical_cast.hpp>

using namespace Wt;

MathduelServer::MathduelServer(WServer& server)
  : server_(server)
{ }

bool MathduelServer::connect(Client *client,
             const MathduelEventCallback& handleEvent)
{
  boost::recursive_mutex::scoped_lock lock(mutex_);

  if (clients_.count(client) == 0) {
    ClientInfo clientInfo;

    clientInfo.sessionId = WApplication::instance()->sessionId();
    clientInfo.eventCallback = handleEvent;

    clients_[client] = clientInfo;

    return true;
  } else
    return false;
}

bool MathduelServer::disconnect(Client *client)
{
  boost::recursive_mutex::scoped_lock lock(mutex_);

  return clients_.erase(client) == 1;
}

bool MathduelServer::login(const WString& user)
{
  boost::recursive_mutex::scoped_lock lock(mutex_);

  if (users_.find(user) == users_.end()) {
    users_.insert(user);

    postMathduelEvent(MathduelEvent(MathduelEvent::Login, user));

    return true;
  } else
    return false;
}

void MathduelServer::logout(const WString& user)
{
  boost::recursive_mutex::scoped_lock lock(mutex_);

  UserSet::iterator i = users_.find(user);

  if (i != users_.end()) {
    users_.erase(i);

    postMathduelEvent(MathduelEvent(MathduelEvent::Logout, user));
  }
}

bool MathduelServer::createGame(const WString& gameName, 
				const WString& hostUserName, long maxRound, const InGameEventCallback& handleEvent)
{
  boost::recursive_mutex::scoped_lock lock(mutex_);
  
  if (games_.count(gameName) == 0) {
	PDuel duelPtr (new Duel(gameName.toUTF8(),hostUserName.toUTF8(),maxRound));
    games_.insert(std::pair<Wt::WString,PDuel>(gameName.toUTF8(),duelPtr));
    
	DuelClientsInfo duelClientsInfo;
    duelClientsInfo.duelClientsInfoMap.insert(std::pair<std::string, 
			InGameEventCallback>(WApplication::instance()->sessionId(), 
			handleEvent));
    duelClients_[gameName] = duelClientsInfo;
    postMathduelEvent(MathduelEvent(MathduelEvent::CreateGame, hostUserName, gameName));
    return true;
  } else
    return false;
}

bool MathduelServer::joinGame(const WString& gameName, 
				const WString& guestUserName, const InGameEventCallback& handleEvent)
{
  boost::recursive_mutex::scoped_lock lock(mutex_);
  
  if (games_.count(gameName) != 0) {
    PDuel duel = games_[gameName];
    duel->joinGame(guestUserName.toUTF8());
    duelClients_[gameName].duelClientsInfoMap.insert(std::pair<std::string, 
			InGameEventCallback>(WApplication::instance()->sessionId(), 
			handleEvent));

    postMathduelEvent(MathduelEvent(MathduelEvent::GameUnavailable, guestUserName, gameName));
    // when joining game gameName is sent as data to avoid race conditions.
    postIngameEvent(InGameEvent(InGameEvent::JoinGame, guestUserName, gameName),gameName);
	    
    return true;
  } else
    return false;
}

bool MathduelServer::guestPlayerReady(const WString& gameName, const WString& guestUserName)
{
  boost::recursive_mutex::scoped_lock lock(mutex_);
  if (games_.count(gameName) != 0) {
	games_[gameName]->setUserReadyForNextRound(guestUserName.toUTF8());
    postIngameEvent(InGameEvent(InGameEvent::GuestPlayerReady, guestUserName, gameName),gameName);    
    return true;
  } else
    return false;
}

bool MathduelServer::gameStarted(const WString& gameName, const WString& hostUserName)
{
  boost::recursive_mutex::scoped_lock lock(mutex_);
  if (games_.count(gameName) != 0) {
	games_[gameName]->setUserReadyForNextRound(hostUserName.toUTF8());
    postIngameEvent(InGameEvent(InGameEvent::GameStarted, hostUserName, gameName),gameName);
    return true;
  } else
    return false;
}

bool MathduelServer::submitResult(const WString& gameName, const WString& userName, const double& result)
{
  boost::recursive_mutex::scoped_lock lock(mutex_);
  if (games_.count(gameName) != 0) {
	PDuel duel = games_[gameName];
	
	if(!duel->isResultSubmitted()) {
		duel->setResultSubmitted(true);
		bool resultCorrect = duel->submitAnswer(userName.toUTF8(), result);
		
		if(duel->getCurrentRound() < duel->getMaxRounds()) {
			postIngameEvent(
				InGameEvent(
					InGameEvent::ResultSubmitted,
					userName,
					boost::lexical_cast<std::string>(resultCorrect)),
				gameName
			);
		}
		else {
			std::string winnerName;
			if(duel->getHostUserScore() > duel->getJoinedUserScore())
				winnerName = duel->getHostUserName();
			else if(duel->getHostUserScore() < duel->getJoinedUserScore())
				winnerName = duel->getJoinedUserName();
			else
				winnerName = "draw";
			postIngameEvent(
					InGameEvent(
						InGameEvent::GameEnded,
						userName,
						winnerName),
					gameName
				);
		}
	}
	
	
    return true;
  } else
    return false;
}

bool MathduelServer::readyForNextRound(const WString& gameName, const WString& userName)
{
  boost::recursive_mutex::scoped_lock lock(mutex_);
  if (games_.count(gameName) != 0) {
	games_[gameName]->setUserReadyForNextRound(userName.toUTF8());
    postIngameEvent(InGameEvent(InGameEvent::PlayerReady, userName, gameName),gameName);
    return true;
  } else
    return false;
}

bool MathduelServer::closeGame(const WString& gameName, const WString& userName)
{	
  boost::recursive_mutex::scoped_lock lock(mutex_);
  if (games_.count(gameName) != 0) {
	duelClients_[gameName].duelClientsInfoMap
					.erase(WApplication::instance()->sessionId());
	if(duelClients_[gameName].duelClientsInfoMap.empty())
	{
		games_.erase(gameName);
		duelClients_.erase(gameName);
	}
	postIngameEvent(InGameEvent(InGameEvent::QuitGame, userName, gameName),gameName);

	return true;
  } else
	return false;
}

bool MathduelServer::changeName(const WString& user, const WString& newUser)
{
  if (user == newUser)
    return true;

  boost::recursive_mutex::scoped_lock lock(mutex_);

  UserSet::iterator i = users_.find(user);

  if (i != users_.end()) {
    if (users_.count(newUser) == 0) {
      users_.erase(i);
      users_.insert(newUser);

      postMathduelEvent(MathduelEvent(MathduelEvent::Rename, user, newUser));

      return true;
    } else
      return false;
  } else
    return false;
}

WString MathduelServer::suggestGuest()
{
  boost::recursive_mutex::scoped_lock lock(mutex_);

  for (int i = 1;; ++i) {
    std::string s = "guest " + boost::lexical_cast<std::string>(i);
    WString ss = s;

    if (users_.find(ss) == users_.end())
      return ss;
  }
}

/*void MathduelServer::sendMessage(const WString& user, const WString& message)
{
  postMathduelEvent(MathduelEvent(user, message));
}*/

void MathduelServer::postMathduelEvent(const MathduelEvent& event)
{
  boost::recursive_mutex::scoped_lock lock(mutex_);

  WApplication *app = WApplication::instance();

  for (ClientMap::const_iterator i = clients_.begin(); i != clients_.end();
       ++i) {
    /*
     * If the user corresponds to the current application, we directly
     * call the call back method. This avoids an unnecessary delay for
     * the update to the user causing the event.
     *
     * For other uses, we post it to their session. By posting the
     * event, we avoid dead-lock scenarios, race conditions, and
     * delivering the event to a session that is just about to be
     * terminated.
     */
    if (app && app->sessionId() == i->second.sessionId)
      i->second.eventCallback(event);
    else
      server_.post(i->second.sessionId,
       boost::bind(i->second.eventCallback, event));
  }
}

void MathduelServer::postIngameEvent(const InGameEvent& event, const WString& gameName)
{
  boost::recursive_mutex::scoped_lock lock(mutex_);

  WApplication *app = WApplication::instance();
  
  DuelClientsInfo duelClients = duelClients_[gameName];
	 
  for (std::map<std::string, InGameEventCallback>::const_iterator 
				i = duelClients.duelClientsInfoMap.begin(); 
				i != duelClients.duelClientsInfoMap.end();
       ++i) {
    if (app && app->sessionId() == i->first)
      i->second(event);
    else
      server_.post(i->first,
       boost::bind(i->second, event));
       
  }
}


MathduelServer::UserSet MathduelServer::users()
{
  boost::recursive_mutex::scoped_lock lock(mutex_);

  UserSet result = users_;

  return result;
}


std::set<Wt::WString> MathduelServer::games()
{
 boost::recursive_mutex::scoped_lock lock(mutex_);

  std::set<Wt::WString> result;
 
  for ( auto game : games_ )
  {
	if(game.second->isDuelAvailable())
      result.insert(game.first);
  }

  return result;
}

std::shared_ptr<GameInfo> MathduelServer::getGameInfo(const Wt::WString& gameName)
{
	PDuel duelPtr = games_.find(gameName)->second;
	std::shared_ptr<GameInfo> gameInfoPtr(new GameInfo());
	
	gameInfoPtr->expression = duelPtr->getExpression();
	gameInfoPtr->gameName = gameName;
	gameInfoPtr->hostUserName = duelPtr->getHostUserName();
	gameInfoPtr->joinedUserName = duelPtr->getJoinedUserName();
	gameInfoPtr->hostUserScore = boost::lexical_cast<std::string>(duelPtr->getHostUserScore());
	gameInfoPtr->joinedUserScore = boost::lexical_cast<std::string>(duelPtr->getJoinedUserScore());
	gameInfoPtr->currentRound = boost::lexical_cast<std::string>(duelPtr->getCurrentRound());
	gameInfoPtr->maxRound = boost::lexical_cast<std::string>(duelPtr->getMaxRounds());
	
	return gameInfoPtr;
}


