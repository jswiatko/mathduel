#include <Wt/WApplication>
#include <Wt/WContainerWidget>
#include <Wt/WEnvironment>
#include <Wt/WHBoxLayout>
#include <Wt/WVBoxLayout>
#include <Wt/WLabel>
#include <Wt/WLineEdit>
#include <Wt/WText>
#include <Wt/WTextArea>
#include <Wt/WPushButton>
#include <Wt/WCheckBox>
#include <Wt/WGroupBox>
#include <Wt/WTable>

#include <libmathduel/view/LayoutBuilder.h>

using namespace Wt;

Wt::WVBoxLayout * LayoutBuilder::createLoginLayout(Wt::WLineEdit *userNameEdit, Wt::WWidget *loginButton, Wt::WText *statusMsg)
{
  WVBoxLayout *vLayout = new WVBoxLayout();
  WHBoxLayout *hLayout = new WHBoxLayout();
  vLayout->addLayout(hLayout, 0, AlignTop | AlignLeft);
  hLayout->addWidget(new WLabel("User name:"), 0, AlignMiddle);
  hLayout->addWidget(userNameEdit, 0, AlignMiddle);
  userNameEdit->setFocus();
  hLayout->addWidget(loginButton, 0, AlignMiddle);
  vLayout->addWidget(statusMsg);
  statusMsg->setTextFormat(PlainText);
  
  return vLayout;
}

Wt::WVBoxLayout * LayoutBuilder::createMenuLayout(Wt::WWidget *gameList, Wt::WWidget *createGameButton, Wt::WWidget *logoutButton, Wt::WText *statusMsg)
{
	WVBoxLayout *vLayout = new WVBoxLayout();
	WHBoxLayout *hLayout = new WHBoxLayout();
	
	hLayout->addWidget(gameList);
	gameList->setStyleClass("chat-users");
	hLayout->setResizable(0, true);
	vLayout->addLayout(hLayout, 1);
	hLayout = new WHBoxLayout();
	hLayout->addWidget(createGameButton);
	hLayout->addWidget(logoutButton);
	vLayout->addLayout(hLayout, 0, AlignLeft);
	
	vLayout->addWidget(statusMsg);
	statusMsg->setTextFormat(PlainText);
	
	
	return vLayout;
}

Wt::WVBoxLayout * LayoutBuilder::createGameLayout(WHBoxLayout *duelingGroundLayout, Wt::WWidget *startGameButton, Wt::WWidget *exitGame, Wt::WText *statusMsg)
{
  WVBoxLayout *vLayout = new WVBoxLayout();
  WHBoxLayout *hLayout = new WHBoxLayout();

  vLayout->addLayout(duelingGroundLayout, 1);
  
  hLayout->addWidget(startGameButton);
  hLayout->addWidget(exitGame);
  vLayout->addLayout(hLayout, 0, AlignLeft);
  
  vLayout->addWidget(statusMsg);
  statusMsg->setTextFormat(PlainText);

  return vLayout;
}

Wt::WHBoxLayout * LayoutBuilder::createDuelingGroundLayout(DuelingGroundLayoutType type, GameWidgets *gameWidgets, WWidget *button, WWidget *text)
{
	WHBoxLayout *duelingGroundLayout = new WHBoxLayout();
	WTable *gameInfoTable = new WTable();
	WGroupBox *problemGroup = new WGroupBox("Dueling Ground");
	
	duelingGroundLayout->addWidget(problemGroup, 1);
	duelingGroundLayout->addWidget(gameInfoTable, 0);
	
	fillGameInfoTable(gameInfoTable, gameWidgets);
	
	problemGroup->setLayout(createProblemGroupLayout(type, gameWidgets, button, text));
	
	return duelingGroundLayout;
}

void LayoutBuilder::fillGameInfoTable(WTable * gameInfoTable, GameWidgets * gameWidgets)
{
	gameInfoTable->elementAt(0,0)->addWidget(gameWidgets->gameName);
	gameInfoTable->elementAt(1,0)->addWidget(new WText("Current round:"));
	gameInfoTable->elementAt(1,1)->addWidget(gameWidgets->currentRound);
	gameInfoTable->elementAt(2,0)->addWidget(new WText("Max rounds:"));
	gameInfoTable->elementAt(2,1)->addWidget(gameWidgets->maxRound);
	gameInfoTable->elementAt(3,0)->addWidget(new WText("Host user:"));
	gameInfoTable->elementAt(3,1)->addWidget(gameWidgets->hostUserName);
	gameInfoTable->elementAt(4,0)->addWidget(new WText("Guest user:"));
	gameInfoTable->elementAt(4,1)->addWidget(gameWidgets->joinedUserName);
	gameInfoTable->elementAt(5,0)->addWidget(new WText("Host's score:"));
	gameInfoTable->elementAt(5,1)->addWidget(gameWidgets->hostUserScore);
	gameInfoTable->elementAt(6,0)->addWidget(new WText("Guest's score:"));
	gameInfoTable->elementAt(6,1)->addWidget(gameWidgets->joinedUserScore);
	
	for(int i=1 ; i <= 6 ; i++)
		gameInfoTable->elementAt(i,1)->setContentAlignment(AlignRight);
}

WVBoxLayout* LayoutBuilder::createProblemGroupLayout(DuelingGroundLayoutType type, GameWidgets *gameWidgets, WWidget *button, WWidget *text)
{
	if(type == HostWaitingForConnect)
		return createProblemGroupLayoutWithMessage("Waiting for an opponent...");
	else if(type == WaitingForOpponent)
		return createProblemGroupLayoutWithMessage("Opponent preparing...");
	else if(type == GuestPreparing)
		return createProblemGroupLayoutWithMessage("Click 'Ready!' button when ready!");
	else if(type == GuestWaiting)
		return createProblemGroupLayoutWithMessage("Waiting for host to start the game...");
	else if(type == HostPreparing)
		return createProblemGroupLayoutWithMessage("Click 'Start game' button when ready!");
	else if(type == InGame)
		return createInGameProblemGroupLayout(gameWidgets, button);
	else if(type == ResultSubmitted)
		return createResultSubmittedProblemGroupLayout(gameWidgets, button, text);
    else if(type == GameEnded)
		return createProblemGroupLayoutWithMessage(text);
	else if(type == OpponentQuit)
		return createProblemGroupLayoutWithMessage("Your opponent quit the game. You win!");	
	else
		return createResultsProblemGroupLayout();
}

WVBoxLayout* LayoutBuilder::createProblemGroupLayoutWithMessage(const WString & message)
{
	WVBoxLayout *problemGroupLayout = new WVBoxLayout();
	problemGroupLayout->addWidget(new WText(message), 1, AlignCenter);
	return problemGroupLayout;
}

WVBoxLayout* LayoutBuilder::createProblemGroupLayoutWithMessage(WWidget *text)
{
	WVBoxLayout *problemGroupLayout = new WVBoxLayout();
	problemGroupLayout->addWidget(text, 1, AlignCenter);
	return problemGroupLayout;
}

WVBoxLayout* LayoutBuilder::createInGameProblemGroupLayout(GameWidgets *gameWidgets, WWidget *button)
{
	WVBoxLayout *problemGroupLayout = new WVBoxLayout();
	
	problemGroupLayout->addWidget(new WText("Calculate the result of given expression:"));
	problemGroupLayout->addWidget(gameWidgets->expression, 1, AlignCenter);
	WContainerWidget *resultWidget = new WContainerWidget();
	problemGroupLayout->addWidget(resultWidget, 1, AlignCenter);
	resultWidget->addWidget(new WText("Result:"));
	resultWidget->addWidget(gameWidgets->result);
	resultWidget->addWidget(button);
	
	return problemGroupLayout;
}

WVBoxLayout* LayoutBuilder::createResultSubmittedProblemGroupLayout(GameWidgets *gameWidgets, WWidget *button, WWidget *text)
{
	WVBoxLayout *problemGroupLayout = new WVBoxLayout();
	
	problemGroupLayout->addWidget(text, 1, AlignCenter);
	problemGroupLayout->addWidget(new WText("Click 'Ready!' button when ready for next round!"), 1, AlignCenter);
	problemGroupLayout->addWidget(button, 0, AlignCenter);
	
	return problemGroupLayout;
}


WVBoxLayout* LayoutBuilder::createResultsProblemGroupLayout()
{
	WVBoxLayout *problemGroupLayout = new WVBoxLayout();
	return problemGroupLayout;
}
