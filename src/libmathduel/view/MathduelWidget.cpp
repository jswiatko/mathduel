#include <libmathduel/view/MathduelWidget.h>
#include <libmathduel/server/MathduelServer.h>

#include <Wt/WApplication>
#include <Wt/WContainerWidget>
#include <Wt/WEnvironment>
#include <Wt/WHBoxLayout>
#include <Wt/WVBoxLayout>
#include <Wt/WLabel>
#include <Wt/WWidget>
#include <Wt/WLineEdit>
#include <Wt/WText>
#include <Wt/WTextArea>
#include <Wt/WPushButton>
#include <Wt/WCheckBox>
#include <Wt/WGroupBox>

#include <iostream>

using namespace Wt;

MathduelWidget::MathduelWidget(MathduelServer& server, Wt::WContainerWidget *parent)
  : WContainerWidget(parent),
    server_(server),
    loggedIn_(false),
    inGame_(false),
    userReadyForNextRound_(false),
    opponentReadyForNextRound_(false),
    gameList_(0),
    gameWidgets_(0)
{
  user_ = server_.suggestGuest();
  letLogin();
}

MathduelWidget::~MathduelWidget()
{
  exitGame();
  logout();
  disconnect();
}

void MathduelWidget::connect()
{
  if (server_.connect
      (this, boost::bind(&MathduelWidget::processMathduelEvent, this, _1)))
    Wt::WApplication::instance()->enableUpdates(true);
}

void MathduelWidget::disconnect()
{
  if (server_.disconnect(this))
    Wt::WApplication::instance()->enableUpdates(false);
}

void MathduelWidget::letLogin()
{
  disconnect();

  clear(); 

  userNameEdit_ = new WLineEdit(user_);
  Wt::WPushButton * loginButton = new WPushButton(("Login"));
  loginButton->clicked().connect(this, &MathduelWidget::login);
	
  
  userNameEdit_->enterPressed().connect(this, &MathduelWidget::login);
  userNameEdit_->setFocus();
  statusMsg_ = new WText();

  setLayout(LayoutBuilder::createLoginLayout(userNameEdit_, loginButton, statusMsg_));
}

void MathduelWidget::login()
{
  if (!loggedIn()) {
    WString name = userNameEdit_->text();
 
  /*
   * When logging in, we pass our processMathduelEvent method as the function that
   * is used to indicate a new mathduel event for this user.
   */
	if (server_.login(name)) {
		loggedIn_ = true;
		connect();
		user_ = name;    
		
		goToMenuPanel();
	} else
    statusMsg_->setText("Sorry, name '" + escapeText(name) +
			  "' is already taken.");
  }
}

void MathduelWidget::logout()
{
	if (loggedIn()) {
		loggedIn_ = false;
		server_.logout(user_);

		letLogin();
	}
}

bool MathduelWidget::loggedIn() const
{
	return loggedIn_;
}

bool MathduelWidget::inGame() const
{
	return inGame_;
}

bool MathduelWidget::isHost(WString &user) const
{
	if(currentGameInfo_->hostUserName == user)
		return true;
	else
		return false;
}

void MathduelWidget::render(WFlags<RenderFlag> flags)
{
  if (flags & RenderFull) {
    if (loggedIn()) {
		//TODO
    }
  }

  WContainerWidget::render(flags);
}


void MathduelWidget::goToMenuPanel()
{
	clear();
	std::cout << "Join game widget 1" << std::endl;
	std::cout << "Join game widget 1" << std::endl;
    userNameEdit_ = 0;
    gameList_ = new WContainerWidget();
    gameList_->setOverflow(WContainerWidget::OverflowAuto);
	Wt::WPushButton * createButton = new WPushButton(("Create new game"));
	createButton->clicked().connect(this, &MathduelWidget::createNewGame);
	Wt::WPushButton * logoutButton = new WPushButton(("Logout"));
	logoutButton->clicked().connect(this, &MathduelWidget::logout);

	statusMsg_ = new WText();
    
    setLayout(LayoutBuilder::createMenuLayout(gameList_, createButton, logoutButton, statusMsg_));

    if (!createButton->parent()) {
			delete gameList_;
			gameList_ = 0;
	}
    if (!gameList_->parent()) {
			delete gameList_;
			gameList_ = 0;
	}
	if (!statusMsg_->parent()) {
			delete gameList_;
			gameList_ = 0;
	}	
    updateGames();
}

void MathduelWidget::goToGamePanel()
{	
	clear();
	/*
	 * If previous gameWidgets_ was created, it's widgets were deleted by clear(),
	 * but gameWidgets_ was not deleted. Delete it now.
	 */
	if(gameWidgets_) {
		delete gameWidgets_;
		gameWidgets_ = 0;
	}

	Wt::WPushButton * exitButton = new WPushButton(("Exit game"));
	exitButton->clicked().connect(this, &MathduelWidget::exitGame);
	gameWidgets_ = new GameWidgets();
	std::shared_ptr<GameInfo> gameInfoPtr(new GameInfo());
	currentGameInfo_ = std::make_shared<GameInfo>();
	gameWidgets_->result->enterPressed().connect(this, &MathduelWidget::submitResult);
	gameWidgets_->result->setFocus();
	statusMsg_ = new WText();
	updateGameInfo();
	duelingGroundLayout_ = new Wt::WHBoxLayout();		
    Wt::WPushButton * gameActionButton;
	if(isHost(user_)) {
		gameActionButton = new WPushButton(("Start game"));
	    gameActionButton->clicked().connect(this, &MathduelWidget::startTheGame);
	    gameActionButton->disable();
	    startGameButton_ = gameActionButton;
		setDuelingGroundLayout(LayoutBuilder::HostWaitingForConnect);
	}
	else {
		gameActionButton = new WPushButton(("Ready!"));
	    gameActionButton->clicked().connect(this, &MathduelWidget::readyForGame);
		readyButton_ = gameActionButton;
		setDuelingGroundLayout(LayoutBuilder::GuestPreparing);
	}
    
	setLayout
	(
		LayoutBuilder::createGameLayout
		(
			duelingGroundLayout_,
			gameActionButton,
			exitButton,
			statusMsg_
		)
	);


	if (!exitButton->parent()) {
			delete exitButton;
			exitButton = 0;
	}
	if (!statusMsg_->parent()) {
			delete statusMsg_;
			statusMsg_ = 0;
	}
	if (!gameActionButton->parent()) {
			delete gameActionButton;
			gameActionButton = 0;
	}
}

void MathduelWidget::setDuelingGroundLayout(LayoutBuilder::DuelingGroundLayoutType type, WText* text)
{
	if(duelingGroundLayout_->count() != 0)
		duelingGroundLayout_->removeItem(duelingGroundLayout_->itemAt(0));
	statusMsg_->setText("");
	
	if(type == LayoutBuilder::InGame) {
		gameWidgets_->result->setText("");
		WPushButton * submitButton = new WPushButton(("Submit result"));
	    submitButton->clicked().connect(this, &MathduelWidget::submitResult);
		duelingGroundLayout_->addItem(
			LayoutBuilder::createDuelingGroundLayout(
				LayoutBuilder::InGame,
				gameWidgets_,
				submitButton
			)
		);
	}
	else if(type == LayoutBuilder::ResultSubmitted) {
		WPushButton * inGameReadyButton = new WPushButton(("Ready!"));
	    inGameReadyButton->clicked().connect(this, &MathduelWidget::readyForNextRound);
		inGameReadyButton->setFocus();
		duelingGroundLayout_->addItem(
			LayoutBuilder::createDuelingGroundLayout(
				LayoutBuilder::ResultSubmitted,
				gameWidgets_,
				inGameReadyButton,
				text
			)
		);
	}
	else if(type == LayoutBuilder::GameEnded)
		duelingGroundLayout_->addItem(
			LayoutBuilder::createDuelingGroundLayout(
					LayoutBuilder::GameEnded,
					gameWidgets_,
					new WPushButton(),
					text
				)
		);
	else {
		duelingGroundLayout_->addItem(
			LayoutBuilder::createDuelingGroundLayout(
				type,
				gameWidgets_
			)
		);
	}
}

void MathduelWidget::startTheGame()
{
	if (server_.gameStarted(currentGameInfo_->gameName, user_)) {
		setDuelingGroundLayout(LayoutBuilder::InGame);
		if(startGameButton_)
			startGameButton_->disable();
	} else
		statusMsg_->setText("Sorry, something went wrong"); 
}

void MathduelWidget::readyForGame()
{
	if (server_.guestPlayerReady(currentGameInfo_->gameName, user_)) {
		setDuelingGroundLayout(LayoutBuilder::GuestWaiting);
		if(readyButton_)
			readyButton_->disable();
	} else
		statusMsg_->setText("Sorry, something went wrong"); 
}

void MathduelWidget::readyForNextRound()
{
	setDuelingGroundLayout(LayoutBuilder::WaitingForOpponent);
	userReadyForNextRound_ = true;
	if (!server_.readyForNextRound(currentGameInfo_->gameName, user_))
		statusMsg_->setText("Sorry, something went wrong"); 
}

void MathduelWidget::exitGame()
{
	if (!server_.closeGame(currentGameInfo_->gameName,user_)) 
		statusMsg_->setText("Server failed to close the game"); 
	inGame_ = false;
	goToMenuPanel();
}

void MathduelWidget::submitResult()
{
	try {
		double result = boost::lexical_cast<double>(gameWidgets_->result->text());
		if (!server_.submitResult(currentGameInfo_->gameName, user_, result))
			statusMsg_->setText("Sorry, something went wrong");
	}
	catch(boost::bad_lexical_cast) {
		gameWidgets_->result->setText("");
		statusMsg_->setText("Please, submit result in format '[0-9]+' or '[0-9]+.[0-9]+'");
	}
}

void MathduelWidget::createNewGame()
{
	Wt::WString gameName = user_ + "'s game";
	if (server_.createGame(gameName,user_,2, boost::bind(&MathduelWidget::processIngameEvent, this, _1))) {
		inGame_ = true;
		gameName_ = gameName;
		goToGamePanel();
	} else
	{
      statusMsg_->setText("Sorry, couldn't create new game");
  }
}

void MathduelWidget::updateGames()
{	
  if (gameList_) {
	gameList_->clear();

    std::set<Wt::WString> games = server_.games();
    
    for (std::set<Wt::WString>::iterator i = games.begin();
	  i != games.end(); ++i) {
		WGroupBox *listElement = new WGroupBox(gameList_);
		listElement->setInline(false);
		listElement->addWidget(new WText(escapeText(*i)));
		WPushButton *joinButton = new WPushButton("Join", listElement);
		joinButton->clicked().connect(boost::bind(&MathduelWidget::joinGame,this,escapeText(*i)));
    }
  }
}

void MathduelWidget::joinGame(const WString & gameName)
{
	if (server_.joinGame(gameName,user_,boost::bind(&MathduelWidget::processIngameEvent, this, _1))) {
		gameName_ = gameName;
		updateGameInfo();
		inGame_ = true;
		goToGamePanel();
	} else
      statusMsg_->setText("Sorry, couldn't join game");
}

void MathduelWidget::updateGameInfo()
{
	if(inGame_) {
		currentGameInfo_ = server_.getGameInfo(gameName_);
		currentGameInfo_->expression;
		gameWidgets_->expression->setText(currentGameInfo_->expression);
		gameWidgets_->gameName->setText(currentGameInfo_->gameName);
		gameWidgets_->hostUserName->setText(currentGameInfo_->hostUserName);
		gameWidgets_->joinedUserName->setText(currentGameInfo_->joinedUserName);
		gameWidgets_->hostUserScore->setText(currentGameInfo_->hostUserScore);
		gameWidgets_->joinedUserScore->setText(currentGameInfo_->joinedUserScore);
		gameWidgets_->currentRound->setText(currentGameInfo_->currentRound);
		gameWidgets_->maxRound->setText(currentGameInfo_->maxRound);
	}
}



void MathduelWidget::processMathduelEvent(const MathduelEvent& event)
{
	WApplication *app = WApplication::instance();

	/*
	* This is where the "server-push" happens. The chat server posts to this
	* event from other sessions, see MathduelServer::postMathduelEvent()
	*/
	if (event.type() == MathduelEvent::Rename && event.user() == user_)
	  user_ = event.data();
	
	if(!inGame())
		updateGames();
	app->triggerUpdate();
}

void MathduelWidget::processIngameEvent(const InGameEvent& event)
{
	WApplication *app = WApplication::instance();
    if(inGame_)
		updateGameInfo();
	if(event.type() == InGameEvent::PlayerReady) {
		if(event.user() != user_)
			opponentReadyForNextRound_ = true;
			
		if(bothPlayersReady()) {
			userReadyForNextRound_ = false;
			opponentReadyForNextRound_ = false;
			setDuelingGroundLayout(LayoutBuilder::InGame);
		}
	}
	else if(event.type() == InGameEvent::QuitGame && event.data() != user_ && inGame_ == true)
		setDuelingGroundLayout(LayoutBuilder::OpponentQuit);
	else if(event.type() == InGameEvent::GameEnded)
	{
		WText* endGameMessage = new WText(
				buildEndGameMessage(event.data()));
		setDuelingGroundLayout(LayoutBuilder::GameEnded, endGameMessage);
		inGame_ = false;
	}
	else if(event.type() == InGameEvent::ResultSubmitted) {
		WText* correctnessMessage = new WText(
				buildCorrectnessMessage(event.user(), boost::lexical_cast<bool>(event.data())));
		setDuelingGroundLayout(LayoutBuilder::ResultSubmitted, correctnessMessage);
	}
	else if(inGame_) {
		
		if(event.type() == InGameEvent::JoinGame)
			setDuelingGroundLayout(LayoutBuilder::WaitingForOpponent);
		
		else if(isHost(user_) && event.type() == InGameEvent::GuestPlayerReady) {
			setDuelingGroundLayout(LayoutBuilder::HostPreparing);
				if(startGameButton_)
					startGameButton_->enable();
		}
		if(event.type() == InGameEvent::GameStarted && !isHost(user_))
			setDuelingGroundLayout(LayoutBuilder::InGame);
		
			
		updateGameInfo();
	}
	else {
		if(event.type() == InGameEvent::GameStarted)
			setDuelingGroundLayout(LayoutBuilder::InGame);
	}
	/*
	* This is the server push action: we propagate the updated UI to the client,
	* (when the event was triggered by another user)
	*/
	app->triggerUpdate();
}

bool MathduelWidget::bothPlayersReady()
{
	if(userReadyForNextRound_ && opponentReadyForNextRound_)
		return true;
	else
		return false;
}

WString MathduelWidget::buildCorrectnessMessage(const WString& userName, bool resultCorrect)
{
	WString message;
	if(userName == user_)
		message += "You";
	else
		message += "Your opponent";
	message += " submitted ";
	if(resultCorrect)
		message += "correct result.";
	else
		message += "wrong result.";
	return message;
}

WString MathduelWidget::buildEndGameMessage(const WString& winnerName)
{
	WString message;
	if(winnerName == user_)
		message = "You win!";
	else if(winnerName == "draw")
		message = "It is a draw!";
	else
		message = "Your opponent wins.";
	return message;
}
