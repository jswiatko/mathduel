This is a multiplayer web game written in C++. Players compete against each other to solve mathematical equations. 

To compile the project on Unix-like systems run the below command in main project folder:

```
scons
```

To clean generated files run:
```
scons -c
```

Required libraries are specified in SConstruct file.

To run application server on localhost:8080 use the below command in main project folder:
```
./bin/test --docroot . --http-address 0.0.0.0 --http-port 8080
```
