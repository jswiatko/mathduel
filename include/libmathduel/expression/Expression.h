#ifndef EXPRESSION_H
#define EXPRESSION_H

#include <string>
#include <memory>

/*! \brief Klasa opisująca wyrażenie matematyczne.
 * Wewnętrznie wyrażenie reprezentowane jest jako struktura drzewiasta
 * (b-drzewo) i może zostać dołączone do innego wyrażenia jako
 * gałąź (operand) */

class Expression
{
public:
    /*! \brief Konstruktor */
    Expression() {}
    /*! \brief Destruktor */
    virtual ~Expression() {}
    /*! \brief Zwraca wartość liczbową wyrażenia */
    virtual double value() = 0;
    /*! \brief Zwraca reprezentację wyrażenia jako ciąg znaków */
    virtual std::string string() = 0;
    /*! \brief Zwraca poziom działania - korzenia */
    virtual int level() = 0;
};

typedef std::shared_ptr<Expression> PExpression;

#endif /* EXPRESSION_H */
