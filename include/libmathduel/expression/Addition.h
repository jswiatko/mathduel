#ifndef ADDITION_H
#define ADDITION_H

#include <memory>
#include <libmathduel/expression/OperationExpression.h>

class Addition: public OperationExpression
{
    public:
        Addition();
        Addition(PExpression left, PExpression right);
        virtual ~Addition();
        virtual double value();
        virtual int level();
        virtual char operatorChar();
};

typedef std::shared_ptr<Addition> PAddition;

#endif /* ADDITION_H */
