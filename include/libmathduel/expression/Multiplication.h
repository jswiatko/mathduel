#ifndef MULTIPLICATION_H
#define MULTIPLICATION_H

#include <memory>
#include <libmathduel/expression/OperationExpression.h>

class Multiplication: public OperationExpression
{
    public:
        Multiplication();
        Multiplication(PExpression left, PExpression right);
        virtual ~Multiplication();
        virtual double value();
        virtual int level();
        virtual char operatorChar();
};

typedef std::shared_ptr<Multiplication> PMultiplication;

#endif /* MULTIPILICATION_H */
