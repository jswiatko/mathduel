#ifndef SUBTRACTION_H
#define SUBTRACTION_H

#include <memory>
#include <libmathduel/expression/OperationExpression.h>

class Subtraction: public OperationExpression
{
    public:
        Subtraction();
        Subtraction(PExpression left, PExpression right);
        virtual ~Subtraction();
        virtual double value();
        virtual std::string string();
        virtual int level();
        virtual char operatorChar();
};

typedef std::shared_ptr<Subtraction> PSubtraction;

#endif /* SUBTRACTION_H */
