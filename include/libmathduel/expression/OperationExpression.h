#ifndef OPERATIONEXPRESSION_H
#define OPERATIONEXPRESSION_H

#include <memory>
#include <libmathduel/expression/Expression.h>

class OperationExpression: public Expression
{
    public:
        OperationExpression();
        OperationExpression(PExpression left, PExpression right);
        virtual ~OperationExpression();
        virtual double value() = 0;
        virtual std::string string();
        virtual int level() = 0;
        virtual char operatorChar() = 0;

    protected:
        PExpression _left;
        PExpression _right;

    friend class ExpressionBuilder;
};

typedef std::shared_ptr<OperationExpression> POperationExpression;

#endif /* OPERATIONEXPRESSION_H */
