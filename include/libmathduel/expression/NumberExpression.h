#ifndef NUMBEREXPRESSION_H
#define NUMBEREXPRESSION_H

#include <libmathduel/expression/Expression.h>

class NumberExpression: public Expression
{
    public:
        NumberExpression();
        NumberExpression(double value);
        double value();
        std::string string();
        int level();

    private:
        double _value;
        static const int _max = 10;
        static const int _scale = 1;
};

typedef std::shared_ptr<NumberExpression> PNumberExpression;

#endif /* NUMBEREXPRESSION_H */
