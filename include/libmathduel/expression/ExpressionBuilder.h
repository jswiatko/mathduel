#ifndef EXPRESSIONBUILDER_H
#define EXPRESSIONBUILDER_H

#include <libmathduel/expression/Expression.h>
#include <libmathduel/expression/OperationExpression.h>

/*! \brief Klasa ExpressionBuilder służy do generowania zadań
 * tj. wyrażeń matematycznych razem wyliczoną ich wartością liczbową */

class ExpressionBuilder
{
public:
    /*! \brief Metoda generująca wyrażenie.
     *  \arg numbers Liczba członów wyrażenia (liczb) */
    static PExpression makeExpression(int numbers);

private:
    static int fillExpression(POperationExpression addition, int numbers);
    static POperationExpression randomOperationExpression();

    static const int _operationsCount = 3;
};

#endif /* EXPRESSIONBUILDER_H */
