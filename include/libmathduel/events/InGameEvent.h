#pragma once

#include <Wt/WString>
#include <libmathduel/server/MathduelServer.h>

/*! \brief Encapsulate a MathduelEvent.
 */
class InGameEvent
{
public:
  /*! \brief Enumeration for the event type.
   */
  enum InGameEventType { JoinGame, GuestPlayerReady, GameStarted, ResultSubmitted, PlayerReady, QuitGame, GameEnded };

  /*! \brief Get the event type.
   */
  InGameEventType type() const { return type_; }

  /*! \brief Get the user who caused the event.
   */
  const Wt::WString& user() const { return user_; }

  /*! \brief Get the extra data for this event.
   */
  const Wt::WString& data() const { return data_; }

private:
  InGameEventType type_;
  Wt::WString user_;
  Wt::WString data_;

  InGameEvent(InGameEventType type, const Wt::WString& user,
	    const Wt::WString& data = Wt::WString::Empty)
    : type_(type), user_(user), data_(data)
  { }
  
  friend class MathduelServer;
};
