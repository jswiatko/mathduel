#pragma once

#include <Wt/WLineEdit>
#include <Wt/WText>

class GameWidgets
{
	public:
	  Wt::WLineEdit     	*result;
	  Wt::WText		     	*expression;
	  Wt::WText   	        *gameName;
	  Wt::WText				*hostUserName;
	  Wt::WText				*joinedUserName;
	  Wt::WText				*hostUserScore;
	  Wt::WText				*joinedUserScore;
      Wt::WText				*currentRound;
	  Wt::WText				*maxRound;
	  
	  GameWidgets()
	  {
		  result = new Wt::WLineEdit();
		  expression = new Wt::WText();
		  gameName = new Wt::WText();
		  hostUserName = new Wt::WText();
		  joinedUserName = new Wt::WText();
		  hostUserScore = new Wt::WText();
		  joinedUserScore = new Wt::WText();
		  currentRound = new Wt::WText();
		  maxRound = new Wt::WText();
	  }
};
