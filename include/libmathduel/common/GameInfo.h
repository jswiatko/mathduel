#pragma once

#include <Wt/WString>

class GameInfo
{
	public:
	  Wt::WString     	expression;
	  Wt::WString		gameName;
	  Wt::WString		hostUserName;
	  Wt::WString		joinedUserName;
	  Wt::WString		hostUserScore;
	  Wt::WString		joinedUserScore;
      Wt::WString		currentRound;
	  Wt::WString		maxRound;
	  
	/*  GameInfo()
	  {
		  expression = new Wt::WString();
		  gameName = new Wt::WString();
		  hostUserName = new Wt::WString();
		  joinedUserName = new Wt::WString();
		  hostUserScore = new Wt::WString();
		  joinedUserScore = new Wt::WString();
		  currentRound = new Wt::WString();
		  maxRound = new Wt::WString();
	  }*/
};
