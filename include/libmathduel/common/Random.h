#ifndef RANDOM_H
#define RANDOM_H

#include <random>

/*! \brief Klasa generująca liczby losowe */

class Random
{
public:
    /*! \brief Generuje liczbę losową
     *  \arg min Minimalna możliwa wartość
     *  \arg max Maksymalna możliwa wartość */
    static int roll(int min, int max);

private:
    static std::default_random_engine _generator;
};

#endif /* RANDOM_H */
