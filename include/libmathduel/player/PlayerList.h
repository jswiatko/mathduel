#ifndef PLAYERLIST_H
#define PLAYERLIST_H

#include <map>
#include <boost/thread.hpp>
#include <libmathduel/player/Player.h>

/*! \brief Singleton przchowujący listę znanych graczy */
class PlayerList
{
public:
    /*! \brief Zwraca instancję singletona */
    static PlayerList& getInstance();
    /*! \brief Dodaje gracza do listy
     *  \arg player std::shared_ptr wskazujący na gracza */
    void put(PPlayer player);
    /*! \brief Pobiera wskaźnik na gracza
     *  \arg nickname Nazwa gracza */
    PPlayer get(std::string nickname);
    /*! \brief Usuwa gracza z listy
     *  \arg nickname Nazwa gracza */
    PPlayer pop(std::string nickname);
    /*! \brief Sprawdza, czy gracz jest obecny na liście
     *  \arg nickname Nazwa gracza */
    bool isPresent(std::string nickname);

    /* debug */
    void printAll();

private:
    PlayerList();
    PlayerList(const PlayerList&) = delete;
    PlayerList& operator=(const PlayerList&) = delete;

    std::map<std::string, PPlayer> _players;
    boost::mutex _mutex;
};

#endif /* PLAYERLIST_H */
