#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <memory>

/*! \brief Klasa opisująca gracza */

class Player
{
public:
    /*! \brief Konstruktor.
     *  \arg nickName Nazwa gracza */
    Player(const std::string nickName);
    /*! \brief Zwraca nazwę gracza */
    std::string getNickName();
    /*! \brief Zwraca punkty zdobyte przez gracza */
    int getScore();
    /*! \brief Nadaje graczowi punkty
     *  \arg points Liczba punktów do przyznania */
    void awardPoints(int points);

    enum PlayerState {
        OFFLINE,
        IDLE,
        WAITING,
        PLAYING
    } state;

private:
    std::string _nickName;
    int _score;
};

typedef std::shared_ptr<Player> PPlayer;

#endif /* PLAYER_H */
