#ifndef MATHDUELSERVER_H_
#define MATHDUELSERVER_H_

#include "libmathduel/Duel.h"
#include <libmathduel/common/GameInfo.h>
#include <libmathduel/events/MathduelEvent.h>
#include <libmathduel/events/InGameEvent.h>
#include <boost/noncopyable.hpp>

#include <Wt/WSignal>
#include <Wt/WString>

namespace Wt {
  class WServer;
}

#include <set>
#include <map>
#include <boost/thread.hpp>


typedef boost::function<void (const MathduelEvent&)> MathduelEventCallback;
typedef boost::function<void (const InGameEvent&)> InGameEventCallback;

/*! \brief Mathduel server
 */
class MathduelServer : boost::noncopyable
{
public:
  // A reference to a client.
  class Client
  {
  };

  /*! \brief Create a new mathduel server.
   */
  MathduelServer(Wt::WServer& server);

  /*! \brief Connects to the mathduel server.
   *
   * The passed callback method is posted to when a new event is
   * received.
   *
   * Returns whether the client has been connected (or false if the client
   * was already connected).
   */
  bool connect(Client *client, const MathduelEventCallback& handleEvent);

  /*! \brief Disconnect from the mathduel server.
   *
   * Returns whether the client has been disconnected (or false if the client
   * was not connected).
   */  
  bool disconnect(Client *client);

  /*! \brief Try to login with given user name.
   *
   * Returns false if the login was not successful.
   */
  bool login(const Wt::WString& user);

  /*! \brief Logout from the server.
   */
  void logout(const Wt::WString& user);
  
  /*! \brief Try to create game with given game name.
   *
   * Returns false if the create game was not successful.
   */
  bool createGame(const Wt::WString& gameName, 
				const Wt::WString& hostUserName, long maxRound, const InGameEventCallback& handleEvent);

  /*! \brief Try to join game with given game name.
   *
   * Returns false if the join game was not successful.
   */
  bool joinGame(const Wt::WString& gameName, 
				const Wt::WString& guestUserName, const InGameEventCallback& handleEvent);
	
  /*! \brief Notifies host player, that his guest is ready to play.
   *
   * Returns false if operation was not successful.
   */			
  bool guestPlayerReady(const Wt::WString& gameName, const Wt::WString& guestUserName);
				
  /*! \brief Notifies guest player, that his host started the game.
   *
   * Returns false if operation was not successful.
   */			
  bool gameStarted(const Wt::WString& gameName, const Wt::WString& guestUserName);
  
  /*! \brief Sends submitted result to server
   *
   * Returns false if operation was not successful.
   */
  bool submitResult(const Wt::WString& gameName, const Wt::WString& userName, const double& result);
  
  /*! \brief Method allowing to signal readiness for next round.
   *
   * Returns false if operation was not successful.
   */
  bool readyForNextRound(const Wt::WString& gameName, const Wt::WString& userName);
				
  /*! \brief Try to close game with given game name.
   *
   * Returns false if the join game was not successful.
   */
  bool closeGame(const Wt::WString& gameName, const Wt::WString& userName);
				
  /*! \brief Changes the name.
   */
  bool changeName(const Wt::WString& user, const Wt::WString& newUser);

  /*! \brief Get a suggestion for a guest user name.
   */
  Wt::WString suggestGuest();

  /*! \brief Send a message on behalve of a user.
   */
  void sendMessage(const Wt::WString& user, const Wt::WString& message);

  /*! \brief Typedef for a collection of user names.
   */
  typedef std::set<Wt::WString> UserSet;
  
  
  /*! \brief Typedef for a collection of games.
   */
  typedef std::map<Wt::WString,PDuel> GameMap;

  /*! \brief Get the users currently logged in.
   */
  UserSet users();
  
    /*! \brief Get the games currently available in.
   */
  std::set<Wt::WString> games();
  
  /*! \brief Get info about a game with given name.
   */
  std::shared_ptr<GameInfo> getGameInfo(const Wt::WString& gameName);

private:
  struct ClientInfo {
    std::string sessionId;
    MathduelEventCallback eventCallback;
  };

  typedef std::map<Client *, ClientInfo> ClientMap;
  
  struct DuelClientsInfo {
	 std::map<std::string, InGameEventCallback> duelClientsInfoMap;	 
  };
  
  typedef std::map<Wt::WString, DuelClientsInfo> DuelClientsMap;
  
  Wt::WServer& server_;
  boost::recursive_mutex mutex_;
  ClientMap clients_;
  DuelClientsMap duelClients_;
  UserSet users_;
  GameMap games_;

  void postMathduelEvent(const MathduelEvent& event);
  void postIngameEvent(const InGameEvent& event, const Wt::WString& gameName);
};

/*@}*/

#endif // MATHDUELSERVER_H_
