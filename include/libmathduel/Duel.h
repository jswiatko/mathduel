#ifndef DUEL_H_
#define DUEL_H_


#include <string>
#include <memory>
#include <boost/thread.hpp>

#include <libmathduel/player/Player.h>
#include <libmathduel/expression/Expression.h>

/*! \brief Klasa Duel będzie wykorzystana w klasie MathduelServer jako
 * typedef std::set<Duel> DuelSet. Stanowi ona źródło informacji o aktualnym
 * stanie gry oraz dostarcza funkcjonalność generowania pytań oraz sprawdzania
 * odpowiedzi. Duel w konstruktorze otrzymuje swoją nazwę oraz klienta towrzącego
 *  - hosta oraz jego identyfikator Webtoolkit( typedef std::pair<Client *,
 * ClientInfo> MathduelClient;). Ponadto w konstruktorze tworzone jest Expression
 *  i zapisywane jest w polu currentExpression. Duel oczekuje na dołączenie drugiego
 * gracza (joinedUser). Po dołączniu drugiego gracza przez funkcje joinUser ustawia
 * pole isAvailable na false, aby zabezpieczyć przed nadpisaniem dołączonego gracza.
 * Następnie oczekuje na graczy aby potwierdzili swoją gotowość.
 * Gdy oboje gracze potwierzą swoją gotowość numer rundy ustawiany jest na 1.
 * Gdy numer rundy znajduje sie w zakresie 1 - maxRound możliwe jest odpytywanie
 * o Expression. Duel oczekuje na dpowiedzi od graczy. Po otrzymaniu odpowiedzi
 * (metoda submitAswers) sprawdza jej poprawność i nadaje nadawcy (identyfikowanemu
 * po loginie) odpowiednią liczbę punktów. Np. +1 za poprawną odpowiedź, -1 za błędną
 * odpowiedź. Jeśli nie wygenerowano już maksymalnej liczby Expression dla
 * pojedynczego Duel (określone czez maxRound), generowane jest następne Expression
 * i ustawiane jest jako currentExpression.
 *
 */

class Duel
{
public:
  /*! \brief Duel otrzymuje w konstruktorze swoją  nazwę (duelName),
   * login użytkownika hosta (hostUserName) i identyfikator klienta
   * tego użytkownia (MathduelClient). Ponadto podawana jest maksymalna
   * liczba rund.
   */
  Duel(const std::string &duelName, const std::string &hostUserName,
                        long maxRound);


  /*! \brief Get duelName
   */
  std::string getDuelName();

  /*! \brief Sprawdzenie czy można dołączyć do duelu
   */
  bool isDuelAvailable();

  /*! \brief Pobranie loginu hosta
   */
  std::string getHostUserName();

  /*! \brief Pobranie dolączającego użytkownika hosta
   */
  std::string getJoinedUserName();

   /*! \brief Pobranie liczby punktów hosta
   */
  long getHostUserScore();

  /*! \brief Pobranie liczby punktów dolączającego użytkownika
   */
  long getJoinedUserScore();

  /*! \brief Sprawdzenie aktualnej rundy
   */
  long getCurrentRound();

  /*! \brief Sprawdzenie aktualnej rundy
   */
  long getMaxRounds();

  /*! \brief Dołaczenie drugie użytkownika do gry.
   *
   * Dołacza drugiego gracza ustawiając jego pole joinedUserName.
   * Ponadto ustawia pole isAvailable na false, aby zabezpieczyć przed nadpisaniem
   * dołączonego gracza. Następnie oczekuje na graczy aby potwierdzili swoją gotowość.
   */
  bool joinGame(const std::string& joinedUserName);

  /*! \brief Powierdzenie gotowości
   * Ustawia pole isReady odpowiedniemu użytkonikowi identyfikowanegu loginem
   * readyUserName. Gdy oboje gracze potwierzą swoją gotowość numer rundy ustawiany jest na 1.
   */
  void setUserReadyForNextRound(const std::string& readyUserName);

  /*! \brief Przygotowanie zmiennych do następnej rundy
   */
  void beginNextRound();

  /*! \brief Ustawienie pola _resultSubmitted
   */
  void setResultSubmitted(const bool value);

  /*! \brief Sprawdzenie gotowości
   * Zwraca wartość pola isReady dla użytkonika identyfikowanego loginem
   * userName.
   */
  bool isUserReady(const std::string& userName);

  /*! \brief Sprawdzenie, czy któryś z graczy już przesłał odpowiedź.
   */
  bool isResultSubmitted();

  /*! \brief Zwraca string z poleceniem zadania, które znajduje się aktualnie w
   * currentExpression. Gdy numer rundy znajduje sie w zakresie [1,maxRound]
   * możliwe jest odpytywanie o Expression.
   */
  std::string getExpression();

  /*! \brief Zatwierdzenie odpowiedzi
   *
   * Po otrzymaniu odpowiedzi sprawdza jej poprawność i nadaje nadawcy
   * (identyfikowanemu po loginie) odpowiednią liczbę punktów.
   * Np. +1 za poprawną odpowiedź, -1 za błędną odpowiedź. Jeśli nie
   * wygenerowano już maksymalnej liczby Expression dla pojedynczego Duel
   *  (określone czez maxRound), generowane jest następne Expression
   * i ustawiane jest jako currentExpression.
   * Zwraca true, jeśli odpowiedź poprawna, false w przeciwnym wypadku.
   */
  bool submitAnswer(const std::string& userName, double value);

private:
  boost::mutex _mutex;
  std::string _duelName;
  PPlayer _hostPlayer;
  PPlayer _joinedPlayer;
  bool _hostReady;
  bool _joinedReady;
  bool _resultSubmitted;
  long _currentRound;
  long _maxRounds;
  PExpression _currentExpression;
  int _hostPlayerPoints;
  int _joinedPlayerPoints;

};

typedef std::shared_ptr<Duel> PDuel;

#endif //DUEL_H_


