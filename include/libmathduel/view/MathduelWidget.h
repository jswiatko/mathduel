#ifndef MATHDUELWIDGET_H_
#define MATHDUELWIDGET_H_

#include <Wt/WContainerWidget>
#include <Wt/WJavaScript>
#include <Wt/WSound>

#include <libmathduel/server/MathduelServer.h>
#include <libmathduel/common/GameWidgets.h>
#include <libmathduel/common/GameInfo.h>
#include <libmathduel/view/LayoutBuilder.h>

namespace Wt {
  class WApplication;
  class WPushButton;
  class WText;
  class WLineEdit;
  class WTextArea;
}

/*! \brief A self-contained mathduel widget.
 */
class MathduelWidget : public Wt::WContainerWidget,
			 public MathduelServer::Client
{
	public:
		/*! \brief Create a mathduel widget that will connect to the given server.
		*/
		MathduelWidget(MathduelServer& server, Wt::WContainerWidget *parent = 0);

		/*! \brief Delete a mathduel widget.
		*/
		~MathduelWidget();

		void connect();
		void disconnect();


		/*! \brief Show a simple login screen.
		*/
		void letLogin();

		/*! \brief Start a chat for the given user.
		*
		* Returns false if the user could not login.
		*/
		void goToMenuPanel();
		void goToGamePanel();

		void logout();

		MathduelServer& server() { return server_; }
		const Wt::WString& userName() const { return user_; }

		protected:
		virtual void updateGames();
		virtual void render(Wt::WFlags<Wt::RenderFlag> flags);

	protected:
		bool loggedIn() const;
		bool inGame() const;
		bool isHost(Wt::WString &user) const;

	private:
		typedef std::shared_ptr<Wt::WHBoxLayout> PHBoxLayout;
		typedef std::shared_ptr<Wt::WPushButton> PButton;
		typedef std::map<Wt::WString, PButton> ButtonMap;
		ButtonMap buttons_;
		
		typedef std::map<Wt::WString, bool> GameMap;
		GameMap games_;

		MathduelServer&			server_;
		bool					loggedIn_;
		bool					inGame_;
		bool					userReadyForNextRound_;
		bool					opponentReadyForNextRound_;

		Wt::JSlot				clearInput_;

		Wt::WString				user_;
		Wt::WString				gameName_;
		Wt::WLineEdit			*userNameEdit_;
		Wt::WText				*statusMsg_;
		Wt::WContainerWidget	*gameList_;
		Wt::WHBoxLayout			*duelingGroundLayout_;
		std::shared_ptr<GameInfo> currentGameInfo_;
		GameWidgets *			gameWidgets_;
		Wt::WPushButton *		startGameButton_;
		Wt::WPushButton *		readyButton_;

		void login();
		void createNewGame();
		void startTheGame();
		void readyForGame();
		void readyForNextRound();
		void exitGame();
		void joinGame(const Wt::WString & name);
		void submitResult();
		void setButton(const Wt::WString& text, const Wt::WString& name, void(MathduelWidget::* method)());
		void setDuelingGroundLayout(LayoutBuilder::DuelingGroundLayoutType type, Wt::WText* correctness=0);
		void updateGameInfo();
		bool bothPlayersReady();
		Wt::WString buildCorrectnessMessage(const Wt::WString& userName, bool resultCorrect);
		Wt::WString buildEndGameMessage(const Wt::WString& winnerName);
		/* called from another session */
		void processMathduelEvent(const MathduelEvent& event);
		void processIngameEvent(const InGameEvent& event);
};

/*@}*/

#endif // MATHDUELWIDGET

