#pragma once

#include <Wt/WVBoxLayout>
#include <Wt/WContainerWidget>
#include <libmathduel/common/GameWidgets.h>

class LayoutBuilder
{
    public:
		enum DuelingGroundLayoutType {HostWaitingForConnect, WaitingForOpponent, GuestPreparing, GuestWaiting, HostPreparing, InGame, OpponentQuit, ResultSubmitted, Results,GameEnded};
        
		static Wt::WVBoxLayout * createLoginLayout(Wt::WLineEdit *userNameEdit, Wt::WWidget *loginButton, Wt::WText *statusMsg);
		static Wt::WVBoxLayout * createMenuLayout(Wt::WWidget *gameList, Wt::WWidget *createGameButton, Wt::WWidget *logoutButton, Wt::WText *statusMsg);
		static Wt::WVBoxLayout * createGameLayout(Wt::WHBoxLayout *duelingGroundLayout, Wt::WWidget *startGameButton, Wt::WWidget *exitGame, Wt::WText *statusMsg);
		static Wt::WHBoxLayout * createDuelingGroundLayout(DuelingGroundLayoutType type, GameWidgets *gameWidgets, Wt::WWidget *button = 0,Wt::WWidget *text = 0);
    private:
        static void fillGameInfoTable(Wt::WTable * gameInfoTable, GameWidgets *gameWidgets);
        
        static Wt::WVBoxLayout * createProblemGroupLayout(DuelingGroundLayoutType type, GameWidgets *gameWidgets, Wt::WWidget *button, Wt::WWidget *text);
        static Wt::WVBoxLayout * createProblemGroupLayoutWithMessage(const Wt::WString & message);
        static Wt::WVBoxLayout * createProblemGroupLayoutWithMessage(Wt::WWidget * message);
        static Wt::WVBoxLayout * createInGameProblemGroupLayout(GameWidgets *gameWidgets, Wt::WWidget *button);
        static Wt::WVBoxLayout * createResultSubmittedProblemGroupLayout(GameWidgets *gameWidgets, Wt::WWidget *button, Wt::WWidget *text);
        static Wt::WVBoxLayout * createResultsProblemGroupLayout(); //TODO
};

     
